import AuthenticatedLayout  from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/react";
import MuiTable from "@/Components/MuiTable";
import { React } from "react";
import { Container, Grid } from "@mui/material";

const Index = ({auth}) => {
    return (
        
        <AuthenticatedLayout auth={auth} user={auth.user}>
            <Head title="Estudiantes" />
            <Container sx={{ p:4}}>
                <Grid container>
                    <Grid item xs={12}>
                    <MuiTable />
                    </Grid>
                </Grid>
            
            </Container>
            
            
        </AuthenticatedLayout>
    )
}

export default Index